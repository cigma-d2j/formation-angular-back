package org.cigma.formationangular.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@PreAuthorize("hasAuthority('Admin')")
@RestController
@RequestMapping("/producer")
public class ProducerController {

    @GetMapping
    public String testProduce(){
        return "TEST";
    }
}
