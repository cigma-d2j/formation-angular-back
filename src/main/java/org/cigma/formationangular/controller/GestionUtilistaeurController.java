package org.cigma.formationangular.controller;

import org.cigma.formationangular.dto.UserDto;
import org.cigma.formationangular.mapper.UtilistaeurMapper;
import org.cigma.formationangular.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/gestionUtilisateur")
public class GestionUtilistaeurController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private UtilistaeurMapper utilistaeurMapper;

    @GetMapping
    public List<UserDto> getAllUsers(){
        return this.utilistaeurMapper.toDtos(this.accountService.getAllUserAppDesc());
    }
}
