package org.cigma.formationangular.controller;

import org.cigma.formationangular.dto.UserDto;
import org.cigma.formationangular.entity.Utilisateur;
import org.cigma.formationangular.mapper.UtilistaeurMapper;
import org.cigma.formationangular.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscription")
public class UtilistaeurController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private UtilistaeurMapper utilistaeurMapper;

    @PostMapping
    public void saveUser(@RequestBody UserDto userDto){
        this.accountService.saveUser(this.utilistaeurMapper.toEntity(userDto));
        this.accountService.addRoleToUser(userDto.getUsername(), "USER");
    }

    @GetMapping
    public List<UserDto> getAllUsers(){
        return this.utilistaeurMapper.toDtos(this.accountService.getAllUserAppDesc());
    }
}
