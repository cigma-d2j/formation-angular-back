package org.cigma.formationangular.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "utilisateur")
@DynamicInsert
@DynamicUpdate
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nom;

    private String prenom;

    @Column(unique = true)
    private String username;

    private String password;

    @Column(unique = true)
    private String cin;

    private String telephone;

    private String adresse;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Role> roles;

    @OneToOne(fetch = FetchType.LAZY)
    private Panier panier;

    public Utilisateur(Long id, String nom, String prenom, String username, String password, String cin, String telephone, String adresse, List<Role> roles, Panier panier) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.cin = cin;
        this.telephone = telephone;
        this.adresse = adresse;
        this.roles = roles;
        this.panier = panier;
    }

    public Utilisateur() {
        this.roles = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public Utilisateur setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Utilisateur setPassword(String password) {
        this.password = password;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Utilisateur setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNom() {
        return nom;
    }

    public Utilisateur setNom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getPrenom() {
        return prenom;
    }

    public Utilisateur setPrenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public String getCin() {
        return cin;
    }

    public Utilisateur setCin(String cin) {
        this.cin = cin;
        return this;
    }

    public String getTelephone() {
        return telephone;
    }

    public Utilisateur setTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public String getAdresse() {
        return adresse;
    }

    public Utilisateur setAdresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public Utilisateur setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

    public Panier getPanier() {
        return panier;
    }

    public Utilisateur setPanier(Panier panier) {
        this.panier = panier;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utilisateur that = (Utilisateur) o;
        return id.equals(that.id) && nom.equals(that.nom) && prenom.equals(that.prenom) && cin.equals(that.cin) && telephone.equals(that.telephone) && adresse.equals(that.adresse) && roles.equals(that.roles) && panier.equals(that.panier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, cin, telephone, adresse, roles, panier);
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", cin='" + cin + '\'' +
                ", telephone='" + telephone + '\'' +
                ", adresse='" + adresse + '\'' +
                ", roles=" + roles +
                ", panier=" + panier +
                '}';
    }
}
