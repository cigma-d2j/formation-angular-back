package org.cigma.formationangular.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="Panier")
@DynamicInsert
@DynamicUpdate
public class Panier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "panier")
    private List<Article> articles;

    @OneToOne(fetch = FetchType.LAZY)
    private Utilisateur utilisateur;

    public Panier(Long id, List<Article> articles, Utilisateur utilisateur) {
        this.id = id;
        this.articles = articles;
        this.utilisateur = utilisateur;
    }

    public Panier() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Panier panier = (Panier) o;
        return id.equals(panier.id) && articles.equals(panier.articles) && utilisateur.equals(panier.utilisateur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, articles, utilisateur);
    }

    @Override
    public String toString() {
        return "Panier{" +
                "id=" + id +
                ", articles=" + articles +
                ", utilisateur=" + utilisateur +
                '}';
    }
}
