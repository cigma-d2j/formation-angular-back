package org.cigma.formationangular.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "categorie")
@DynamicUpdate
@DynamicInsert
public class Categorie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String designation;

    @OneToMany(mappedBy = "categorie", fetch = FetchType.LAZY)
    private List<Article> articles;

    public Categorie(Long id, String designation, List<Article> articles) {
        this.id = id;
        this.designation = designation;
        this.articles = articles;
    }

    public Categorie() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categorie categorie = (Categorie) o;
        return id.equals(categorie.id) && designation.equals(categorie.designation) && articles.equals(categorie.articles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, designation, articles);
    }

    @Override
    public String toString() {
        return "Categorie{" +
                "id=" + id +
                ", designation='" + designation + '\'' +
                ", articles=" + articles +
                '}';
    }
}
