package org.cigma.formationangular.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Article")
@DynamicInsert
@DynamicUpdate
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String designation;

    private Integer quantite;

    private String img;

    @ManyToOne
    private Panier panier;

    @ManyToOne
    private Categorie categorie;

    public Article(Long id, String designation, Integer quantite, String img, Panier panier, Categorie categorie) {
        this.id = id;
        this.designation = designation;
        this.quantite = quantite;
        this.img = img;
        this.panier = panier;
        this.categorie = categorie;
    }

    public Article() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return id.equals(article.id) && designation.equals(article.designation) && quantite.equals(article.quantite) && img.equals(article.img) && panier.equals(article.panier) && categorie.equals(article.categorie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, designation, quantite, img, panier, categorie);
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", designation='" + designation + '\'' +
                ", quantite=" + quantite +
                ", img='" + img + '\'' +
                ", panier=" + panier +
                ", categorie=" + categorie +
                '}';
    }
}
