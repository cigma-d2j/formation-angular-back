package org.cigma.formationangular.configuration;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtAuthorizationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChaine)
            throws ServletException, IOException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, "
                + "Access-Control-Request-Method, Access-Control-Request-Headers, Authorization" );
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH");
        response.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, "
                + "Access-Control-Allow-Credentials, Authorization");
        if(request.getMethod().equals("OPTIONS")){
            response.setStatus(HttpServletResponse.SC_OK);
        }else{
            String jwtToken = request.getHeader("Authorization");
            if(jwtToken == null || !jwtToken.startsWith("Bearer ")){
                System.out.println("User Steel have no Token -> redirect to generete new one");
                filterChaine.doFilter(request, response); return ;
            }
            Claims claims = Jwts.parser()
                    .setSigningKey("FORMATION-D2J-ANGULAR")
                    .parseClaimsJws(jwtToken.replace("Bearer ", ""))
                    .getBody();
            String username = claims.getSubject();
            @SuppressWarnings("unchecked")
            ArrayList<Map<String, String>> roles = (ArrayList<Map<String, String>>)claims.get("roles");
            Collection<GrantedAuthority> authorities = new ArrayList<>();
            roles.forEach(element -> {
                authorities.add(new SimpleGrantedAuthority(element.get("authority")));
            });
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,null, authorities);
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            filterChaine.doFilter(request, response);
        }
    }
}
