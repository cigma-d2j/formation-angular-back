package org.cigma.formationangular.configuration;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.cigma.formationangular.entity.Utilisateur;
import org.cigma.formationangular.repository.UtilisateurRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private UtilisateurRepository utilisateurRepository;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,UtilisateurRepository utilisateurRepository
    ){
        super();
        this.authenticationManager = authenticationManager;
        this.utilisateurRepository = utilisateurRepository;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        Utilisateur user = null;
        try{
            user = new ObjectMapper().readValue(request.getInputStream(), Utilisateur.class);
            //System.out.println(this.cryptoHelper.cryptoAES(user.getPassword().getBytes("UTF-8")));
            String a = "test";
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException("Probléme attemptAuthentication Exception lever lors de la recuperation du user pour la config JWT");
        }
        return this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        User springUser = (User)authResult.getPrincipal();
        String jwtToken = Jwts.builder()
                .setSubject(springUser.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + 864_000_000))
                .signWith(SignatureAlgorithm.HS512, "FORMATION-D2J-ANGULAR")
                .claim("roles",springUser.getAuthorities())
                .claim("userId", this.utilisateurRepository.findByUsername(springUser.getUsername()).getId())
                .compact();
        response.getWriter().append("Bearer "+jwtToken);
    }
}
