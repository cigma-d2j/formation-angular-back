package org.cigma.formationangular.mapper;

import org.cigma.formationangular.dto.UserDto;
import org.cigma.formationangular.entity.Utilisateur;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UtilistaeurMapper  implements  EntityMapper<UserDto, Utilisateur>{

    @Override
    public UserDto toDto(Utilisateur entity) {
        return new UserDto()
                .setAdresse(entity.getAdresse())
                .setTelephone(entity.getTelephone())
                .setCin(entity.getCin())
                .setUsername(entity.getUsername())
                .setId(entity.getId())
                .setNom(entity.getNom())
                .setPrenom(entity.getPrenom());
    }

    @Override
    public Utilisateur toEntity(UserDto dto) {
        return new Utilisateur().setTelephone(dto.getTelephone())
                .setPassword(dto.getPassword())
                .setCin(dto.getCin())
                .setUsername(dto.getUsername())
                .setTelephone(dto.getTelephone())
                .setNom(dto.getNom())
                .setAdresse(dto.getAdresse());
    }

    @Override
    public List<Utilisateur> toEntities(List<UserDto> dto) {
        return dto.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> toDtos(List<Utilisateur> entity) {
        return entity.stream().map(this::toDto).collect(Collectors.toList());
    }
}
