package org.cigma.formationangular.mapper;

import java.util.List;

public interface EntityMapper<D , E> {

    D toDto(E entity);

    E toEntity(D dto);

    List<E> toEntities(List<D> dto);

    List<D> toDtos(List<E> entity);
}
