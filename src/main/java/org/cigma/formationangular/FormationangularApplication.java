package org.cigma.formationangular;

import org.cigma.formationangular.entity.Role;
import org.cigma.formationangular.entity.Utilisateur;
import org.cigma.formationangular.repository.RoleRepository;
import org.cigma.formationangular.repository.UtilisateurRepository;
import org.cigma.formationangular.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableJpaRepositories
@SpringBootApplication
public class FormationangularApplication implements CommandLineRunner {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private AccountService accountService;

	public static void main(String[] args) {
		SpringApplication.run(FormationangularApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Override
	public void run(String... args) throws Exception {
		 this.accountService.saveUser(new
				Utilisateur().setUsername("admin").setPassword("123456").setAdresse("ynov").setTelephone("0689562321").setCin("AZ452369"));
		 this.accountService.saveUser(new
				 Utilisateur().setUsername("utilisateur").setPassword("password").setAdresse("ynov").setTelephone("0689562321").setCin("AA236514"));
		 this.accountService.saveRole(new Role().setRole("ADMIN"));
		 this.accountService.saveRole(new Role().setRole("USER"));
		 this.accountService.addRoleToUser("admin", "ADMIN");
		 this.accountService.addRoleToUser("admin", "USER");
		 this.accountService.addRoleToUser("utilisateur", "USER");
	}
}
