package org.cigma.formationangular.service.implementation;

import org.cigma.formationangular.entity.Role;
import org.cigma.formationangular.entity.Utilisateur;
import org.cigma.formationangular.repository.RoleRepository;
import org.cigma.formationangular.repository.UtilisateurRepository;
import org.cigma.formationangular.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private UtilisateurRepository utilisateurRepository;
    private RoleRepository roleRepository;

    public AccountServiceImpl(UtilisateurRepository utilisateurRepository, RoleRepository roleRepository){
        this.utilisateurRepository = utilisateurRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public Utilisateur saveUser(Utilisateur user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return this.utilisateurRepository.save(user);
    }

    @Override
    public Role saveRole(Role role) {
        return this.roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        Role role = this.roleRepository.findByRole(roleName);
        Utilisateur user = this.utilisateurRepository.findByUsername(userName);
        user.getRoles().add(role);
        utilisateurRepository.save(user);
    }

    @Override
    public Utilisateur findUserByUsername(String username) {
        return this.utilisateurRepository.findByUsername(username);
    }

    @Override
    public Utilisateur findUserById(Long id) {
        return this.utilisateurRepository.findById(id).get();
    }

    @Override
    public List<Utilisateur> getAllUserAppDesc() {
        return this.utilisateurRepository.findAll();
    }

    @Override
    public Long countUsersInDB() {
        return null;
    }

    @Override
    public Utilisateur updateUser(String nickName, byte[] photo, Long userId) {
        return null;
    }
}
