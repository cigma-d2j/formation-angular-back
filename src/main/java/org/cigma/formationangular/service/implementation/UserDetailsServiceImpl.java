package org.cigma.formationangular.service.implementation;

import org.cigma.formationangular.entity.Utilisateur;
import org.cigma.formationangular.service.AccountService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private AccountService accountService;

    public UserDetailsServiceImpl(AccountService accountService){
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur user = this.accountService.findUserByUsername(username);
        if(user == null) throw new UsernameNotFoundException("le login que vous avez saisie n'existe pas dans la base");
        Collection<GrantedAuthority> autorities = new ArrayList<>();
        user.getRoles().forEach(element -> {
            autorities.add(new SimpleGrantedAuthority(element.getRole()));
        });
        return new User(user.getUsername(), user.getPassword(),autorities);
    }
}
