package org.cigma.formationangular.service;

import org.cigma.formationangular.entity.Role;
import org.cigma.formationangular.entity.Utilisateur;

import java.util.List;

public interface AccountService {

    public Utilisateur saveUser(Utilisateur user);
    public Role saveRole(Role role);
    public void addRoleToUser(String userName,String roleName);
    public Utilisateur findUserByUsername(String username);
    public Utilisateur findUserById(Long id);
    public List<Utilisateur> getAllUserAppDesc();
    public Long countUsersInDB();
    public Utilisateur updateUser(String nickName, byte[] photo, Long userId);
}
