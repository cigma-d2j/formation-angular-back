package org.cigma.formationangular.dto;

import java.io.Serializable;
import java.util.Objects;

public class UserDto implements Serializable {

    private Long id;

    private String nom;

    private String prenom;

    private String username;

    private String password;

    private String cin;

    private String telephone;

    private String adresse;

    public UserDto() {
    }

    public UserDto(Long id, String nom, String prenom, String username, String password, String cin, String telephone, String adresse) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.cin = cin;
        this.telephone = telephone;
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", cin='" + cin + '\'' +
                ", telephone='" + telephone + '\'' +
                ", adresse='" + adresse + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return id.equals(userDto.id) && nom.equals(userDto.nom) && prenom.equals(userDto.prenom) && username.equals(userDto.username) && password.equals(userDto.password) && cin.equals(userDto.cin) && telephone.equals(userDto.telephone) && adresse.equals(userDto.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, username, password, cin, telephone, adresse);
    }

    public Long getId() {
        return id;
    }

    public UserDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNom() {
        return nom;
    }

    public UserDto setNom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getPrenom() {
        return prenom;
    }

    public UserDto setPrenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getCin() {
        return cin;
    }

    public UserDto setCin(String cin) {
        this.cin = cin;
        return this;
    }

    public String getTelephone() {
        return telephone;
    }

    public UserDto setTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public String getAdresse() {
        return adresse;
    }

    public UserDto setAdresse(String adresse) {
        this.adresse = adresse;
        return this;
    }
}
